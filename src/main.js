import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import router from './router'
import store from './store';
import * as fb from 'firebase/app';
import BuyDialog from "./components/common/BuyDialog";

Vue.config.productionTip = false;
Vue.component('app-buy-dialog', BuyDialog);

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App),
  created() {
    // Initialize Firebase
    var firebaseConfig = {
      apiKey: "AIzaSyAhOoSo10vU7GkeZBhfsUjrmJKmgZ6MJEc",
      authDomain: "onlinestore-da10d.firebaseapp.com",
      databaseURL: "https://onlinestore-da10d.firebaseio.com",
      projectId: "onlinestore-da10d",
      storageBucket: "onlinestore-da10d.appspot.com",
      messagingSenderId: "881148758037",
      appId: "1:881148758037:web:9e507bea6349a4360ae61c"
    };
    // Initialize Firebase
    fb.initializeApp(firebaseConfig);

    fb.auth().onAuthStateChanged(user => {
      if(user) {
        this.$store.dispatch('autoLoginUser', user)
      }
    });

    this.$store.dispatch('fetchProducts')
  }
}).$mount('#app');
