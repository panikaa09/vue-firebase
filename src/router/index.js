import Vue from 'vue';
import VueRouter from 'vue-router';
import AuthGuard from './auth-guard';
import Home from '@/views/Home.vue';
import Product from '@/views/Products/Product.vue';
import ProductList from '@/views/Products/ProductList.vue';
import NewProduct from '@/views/Products/NewProduct.vue';
import Login from '@/views/Auth/Login.vue';
import Register from '@/views/Auth/Register.vue';
import Checkout from "@/views/User/Checkout";

Vue.use(VueRouter);

const routes = [
  {
    path: '',
    name: 'Home',
    component: Home
  },
  {
    path: '/product/:id',
    props: true,
    name: 'product',
    component: Product
  },
  {
    path: '/list',
    name: 'list',
    component: ProductList,
    beforeEnter: AuthGuard
  },
  {
    path: '/new',
    name: 'new',
    component: NewProduct,
    beforeEnter: AuthGuard
  },
  {
    path: '/checkout',
    name: 'checkout',
    component: Checkout,
    beforeEnter: AuthGuard
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router
